use structopt::StructOpt;
use palette::{Pixel, Srgb, Hsv};
use async_std::net::UdpSocket;
use async_std::{io, task};

mod sk6812rgbw;
use sk6812rgbw::Sk6812Rgbw;

mod grid;
use grid::Grid;

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(short, long, env)]
    spi: String,
    #[structopt(subcommand)]
    cmd: Cmd,
}

#[derive(StructOpt, Debug)]
enum Cmd {
    Test,
    Listen {
        #[structopt(short, long, env)]
        port: u16,
    },
}

fn test(leds: &mut Sk6812Rgbw) {
    for t in 0.. {
        println!("Frame {}", t);
        let frame = Grid::generate(|x, y| {
            let v = (x + y) as f32 * 10.0 + (t as f32) * 5.0;
            let rgb: [u8; 3] = Srgb::from(Hsv::new(v, 1.0, 1.0)).into_format().into_raw();
            [rgb[0], rgb[1], rgb[2], 100]
        });
        leds.write(&frame).unwrap();
        std::thread::sleep(std::time::Duration::from_millis(10));
    }
}

async fn listen(leds: &mut Sk6812Rgbw, port: u16) -> io::Result<()> {
    let socket = UdpSocket::bind(("0.0.0.0", port)).await?;
    let mut buf = vec![0u8; 1024];

    loop {
        let n = socket.recv(&mut buf).await?;
        let rcvd = &buf[..n];
        println!("Received {} bytes", n);
        if rcvd.len() == 4*12*12 {
            let frame: Vec<_> = rcvd.chunks_exact(4)
                .map(|c| [c[0], c[1], c[2], c[3]])
                .collect();
            leds.write(&frame)?;
        }
    }
}

fn main() {
    let opts = Opt::from_args();
    let mut leds = Sk6812Rgbw::open(&opts.spi).unwrap();

    match opts.cmd {
        Cmd::Test => test(&mut leds),
        Cmd::Listen { port } => task::block_on(listen(&mut leds, port)).unwrap(),
    }
}
