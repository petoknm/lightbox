use spidev::{Spidev, SpidevOptions, SpidevTransfer, SpiModeFlags};
use byteorder::{BigEndian, WriteBytesExt};
use std::io::Result;

type Rgbw = [u8; 4];

pub struct Sk6812Rgbw {
    spi: Spidev,
}

const BIT_PATTERN_1: [u8; 2] = [0b1000, 0b1100];
const BIT_PATTERN_2: [u8; 4] = [
    BIT_PATTERN_1[0] << 4 | BIT_PATTERN_1[0],
    BIT_PATTERN_1[0] << 4 | BIT_PATTERN_1[1],
    BIT_PATTERN_1[1] << 4 | BIT_PATTERN_1[0],
    BIT_PATTERN_1[1] << 4 | BIT_PATTERN_1[1],
];

fn bit_pattern_8(byte: u8) -> u32 {
    let byte: usize = byte as usize;
    (BIT_PATTERN_2[(byte >> 6) & 0b11] as u32) << 24 |
    (BIT_PATTERN_2[(byte >> 4) & 0b11] as u32) << 16 |
    (BIT_PATTERN_2[(byte >> 2) & 0b11] as u32) << 8  |
    (BIT_PATTERN_2[(byte >> 0) & 0b11] as u32) << 0
}

lazy_static::lazy_static! {
    static ref BIT_PATTERN_8: Vec<u32> = (0..=255).map(bit_pattern_8).collect();
}

impl Sk6812Rgbw {
    pub fn open(path: &str) -> Result<Sk6812Rgbw> {
        let mut spi = Spidev::open(path)?;
        let options = SpidevOptions::new()
            .bits_per_word(8)
            .max_speed_hz(4*800_000)
            .mode(SpiModeFlags::SPI_MODE_0)
            .build();
        spi.configure(&options)?;
        Ok(Sk6812Rgbw{ spi })
    }

    pub fn write<P: AsRef<[Rgbw]>>(&mut self, pixels: P) -> Result<()> {
        let mut data = vec![];

        // Write foreword
        for _ in 0..4 {
            data.write_u32::<BigEndian>(0)?;
        }

        // Write pixels in GRBW format
        for pix in pixels.as_ref() {
            data.write_u32::<BigEndian>(BIT_PATTERN_8[pix[1] as usize])?;
            data.write_u32::<BigEndian>(BIT_PATTERN_8[pix[0] as usize])?;
            data.write_u32::<BigEndian>(BIT_PATTERN_8[pix[2] as usize])?;
            data.write_u32::<BigEndian>(BIT_PATTERN_8[pix[3] as usize])?;
        }

        // Write footer
        for _ in 0..16 {
            data.write_u32::<BigEndian>(0)?;
        }

        let mut transfer = SpidevTransfer::write(&data);
        self.spi.transfer(&mut transfer)
    }
}
