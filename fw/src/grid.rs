type Rgbw = [u8; 4];

pub struct Grid {
    data: [Rgbw; 12*12],
}

impl Grid {
    pub fn new() -> Grid {
        Grid { data: [[0; 4]; 12*12] }
    }

    pub fn generate<F>(f: F) -> Grid
    where F: Fn(usize, usize) -> Rgbw
    {
        let mut grid = Grid::new();
        for y in 0..12 {
            for x in 0..12 {
                grid.set(x, y, f(x, y));
            } 
        }
        grid
    }

    pub fn set(&mut self, x: usize, y: usize, pix: Rgbw) {
        self.data[y*12 + x] = pix;
    }
}

impl AsRef<[Rgbw]> for Grid {
    fn as_ref(&self) -> &[Rgbw] {
        &self.data
    }
}
