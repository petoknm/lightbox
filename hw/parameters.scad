// Spacing
segment_count = 6;
segment_width = 33.20;

// Wall
wall_height = 35;

// Other
notch_width = 1.0;
plate_thickness = 0.6;
