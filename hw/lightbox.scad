include <./parameters.scad>

module element() {
    difference() {
        square([segment_width, wall_height]);
        square([notch_width, wall_height/2]);
    }
}

module wall_plate() {
    difference() {
        for(i = [0:segment_count-1]) translate([i*segment_width, 0]) element();
        square([notch_width, wall_height]);
    }
}

linear_extrude(height=plate_thickness) {
    for(i = [0:7]) translate([0, i*(wall_height+1)]) wall_plate();
    for(i = [0:1]) translate([240 + i*(wall_height+1), 35]) rotate(90) wall_plate();
}
