use async_std::io;
use async_std::io::prelude::WriteExt;
use async_std::net::TcpStream;
use std::io::Write;

pub enum Packet {
    Frame { data: Vec<u8> },
    AnimationBegin { name: String },
    AnimationEnd,
    AnimationStart { name: String },
    AnimationStop,
}

impl Packet {
    fn to_write<W: Write>(&self, mut w: W) -> io::Result<()> {
        match self {
            Packet::Frame { ref data } => {
                w.write_all(&[0x00])?;
                w.write_all(data)?;
            },
            Packet::AnimationBegin { ref name } => {
                w.write_all(&[0x10])?;
                w.write_all(name.as_bytes())?;
            },
            Packet::AnimationEnd => {
                w.write_all(&[0x11])?;
            },
            Packet::AnimationStart { name } => {
                w.write_all(&[0x12])?;
                w.write_all(name.as_bytes())?;
            },
            Packet::AnimationStop => {
                w.write_all(&[0x13])?;
            },
        };
        Ok(())
    }
}

pub struct PacketStream(TcpStream);

impl PacketStream {
    pub async fn connect(address: &str) -> io::Result<PacketStream> {
        let tcp = TcpStream::connect(address).await?;
        Ok(PacketStream(tcp))
    }

    pub async fn send(&mut self, pkt: &Packet) -> io::Result<()> {
        let mut data = Vec::new();
        pkt.to_write(&mut data)?;
        let mut encoded = cobs::encode_vec(&data);
        encoded.push(0);
        self.0.write_all(&encoded).await
    }

    pub async fn flush(&mut self) -> io::Result<()> {
        self.0.flush().await
    }

    pub async fn send_f(&mut self, pkt: &Packet) -> io::Result<()> {
        self.send(pkt).await?;
        self.flush().await
    }

    pub async fn batch(&mut self, pkts: &[Packet]) -> io::Result<()> {
        for pkt in pkts {
            self.send(pkt).await?;
        }
        Ok(())
    }

    pub async fn batch_f(&mut self, pkts: &[Packet]) -> io::Result<()> {
        self.batch(pkts).await?;
        self.flush().await
    }
}
