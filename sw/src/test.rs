use async_std::io;
use std::iter;
use std::f32::consts::PI;
use crate::packet::{Packet, PacketStream};

pub async fn run(s: &mut PacketStream) -> io::Result<()> {
    s.send(&Packet::AnimationBegin{ name: "test".into() }).await?;

    let frame_pkts: Vec<_> = (0..100)
        .map(|i| (i as f32) / 100.0)
        .map(|i| (2.0 * PI * i).sin())
        .map(|i| (1.0 + i) / 2.0)
        .map(|i| (255.0 * i) as u8)
        .map(|v| iter::repeat(vec![v, 0, v, 0]).take(12*12).flatten().collect())
        .map(|data| Packet::Frame { data })
        .collect();

    s.batch(&frame_pkts).await?;
    s.batch_f(&[
        Packet::AnimationEnd,
        Packet::AnimationStart { name: "test".into() },
    ]).await
}
