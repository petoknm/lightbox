use structopt::StructOpt;
use async_std::io;

#[derive(StructOpt, Debug)]
pub struct Opt {
    #[structopt(short, long, env)]
    address: String,

    #[structopt(subcommand)]
    cmd: Cmd
}

#[derive(StructOpt, Debug)]
pub enum Cmd {
    Animation(AnimationCmd),
    Test
}

#[derive(StructOpt, Debug)]
pub enum AnimationCmd {
    Start {
        #[structopt(short, long, env)]
        name: String
    },
    Stop,
}

mod packet;
mod animation;
mod test;

use packet::PacketStream;

#[async_std::main]
async fn main() -> io::Result<()> {
    let opt = Opt::from_args();
    let mut stream = PacketStream::connect(&opt.address).await?;

    match opt.cmd {
        Cmd::Test => test::run(&mut stream).await?,
        Cmd::Animation(AnimationCmd::Start{ ref name }) => animation::start(&mut stream, name).await?,
        Cmd::Animation(AnimationCmd::Stop) => animation::stop(&mut stream).await?,
    };

    Ok(())
}
