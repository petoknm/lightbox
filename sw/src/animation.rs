use async_std::io;
use crate::packet::{Packet, PacketStream};

pub async fn start(s: &mut PacketStream, name: &str) -> io::Result<()> {
    s.send_f(&Packet::AnimationStart{ name: name.into() }).await
}

pub async fn stop(s: &mut PacketStream) -> io::Result<()> {
    s.send_f(&Packet::AnimationStop).await
}
