FW_BUILD_TARGET := armv7-unknown-linux-gnueabihf
FW_DEPLOY_USER  := root
FW_DEPLOY_HOST  := orangepizero
FW_DEPLOY_SPI   := /dev/spidev1.0
FW_SSH_HOST     := "${FW_DEPLOY_USER}@${FW_DEPLOY_HOST}"

.PHONY: run build deploy

run: deploy
	ssh ${FW_SSH_HOST} lightbox -s ${FW_DEPLOY_SPI} listen -p 7777

deploy: build
	ssh ${FW_SSH_HOST} pkill lightbox
	scp target/${FW_BUILD_TARGET}/release/lightbox ${FW_SSH_HOST}:/usr/local/bin/lightbox

build:
	cargo build --release --package sw
	cargo build --release --package fw --target ${FW_BUILD_TARGET}
